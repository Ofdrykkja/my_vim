syntax on
set termencoding=utf-8
set encoding=utf-8
set fencs=utf-8,cp1251,koi8-r,ucs-2,cp866
set ffs=unix,dos,mac "<EOL> type
set tabstop=4 "count of space for indent
set shiftwidth=4 "count of space for >> <<
set et "replace tab to spaces
set smarttab "added tab==shiftwidt at begining of the line
set ai "autoindent for new lines
set smartindent
set wrap "wrap long lines
set modeline
set ls=2 "allways show status line
set linebreak
set showmatch "show pair of brackets
set hlsearch "highlight search matches
set incsearch
set ignorecase "search ic
"show hidden characters
"set listchars=tab:→→,eol:↲,nbsp:·,trail:·
"set list
set number "line number
set nocompatible
set background=dark
set t_Co=256
"set autochdir
colorscheme herald
"filetype on
"filetype plugin on
"vundle
filetype off
set rtp+=~/.vim/bundle/vundle/
call vundle#rc()
Bundle 'gmarik/vundle'
Bundle 'scrooloose/nerdtree'
Bundle 'scrooloose/nerdcommenter'
"Bundle 'garbas/vim-snipmate'
Bundle 'msanders/snipmate.vim'
Bundle 'Townk/vim-autoclose'
Bundle 'vim-scripts/errormarker.vim'
Bundle 'ervandew/supertab'
Bundle 'javacomplete'
"Bundle 'int3/vim-taglist-plus'
Bundle 'majutsushi/tagbar'
"Bundle 'xolox/vim-session'
Bundle 'othree/xml.vim'
filetype plugin indent on

autocmd bufnewfile *.html so ~/.vim/templates/html
autocmd bufnewfile *.py so ~/.vim/templates/python
autocmd bufnewfile *.xml so ~/.vim/templates/xml
" classname (the filename minus the path and extension)
"iab <buffer> <unique> ,f <c-r>=fnamemodify(getreg('%'), ':r')<CR>
autocmd BufNewFile *.java call InsertJavaPackage()
function! InsertJavaPackage()
  let filename = expand("%")
  let filename = substitute(filename, "\.java$", "", "")
  let dir = getcwd() . "/" . filename
  let dir = substitute(dir, "^.*\/src\/", "", "")
  let dir = substitute(dir, "\/[^\/]*$", "", "")
  let dir = substitute(dir, "\/", ".", "g")
  let filename = substitute(filename, "^.*\/", "", "")
  let dir = "package " . dir . ";"
  let result = append(0, dir)
  let result = append(1, "")
  let result = append(2, "class " . filename . " {")
  let result = append(4, "}")
endfunction
"errormaker
autocmd BufRead,BufNewFile *.java setlocal makeprg=javac\ %
autocmd BufRead,BufNewFile *.java setlocal efm=%f:%l:%m
noremap <Leader>ee :ErrorAtCursor<CR>
"make
autocmd BufRead,BufNewFile *.java nmap <F9> :make<CR>
if filereadable('build.xml') && filereadable('AndroidManifest.xml')
    autocmd BufRead,BufNewFile *.java  setlocal makeprg=ant
endif
"javacomplete
if has("autocmd")
    autocmd Filetype java setlocal omnifunc=javacomplete#Complete
    autocmd Filetype java setlocal completefunc=javacomplete#CompleteParamsInfo
    autocmd Filetype java let g:SuperTabDefaultCompletionType = "<c-x><c-o>"
endif
"auto reload .vimrc
if has("autocmd")
    autocmd! bufwritepost .vimrc source $MYVIMRC
endif
