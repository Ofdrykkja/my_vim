#TODO

1. errormaker (auto)
2. ant compile (:make, :set makeprg=ant) #OK
    * http://habrahabr.ru/post/28362/
    * http://developer.android.com/tools/projects/projects-cmdline.html
    * http://developer.android.com/tools/building/building-cmdline.html
    * http://www.opennet.ru/base/dev/ant-10.txt.html
3. add android omnicomplete #OK
4. add autocomplite #OK * 
    * http://eatmytux.com/vim-auto-completion/
    * https://github.com/anddam/android-javacomplete
    * http://www.ugolnik.info/?p=1646
5. auto close scope, quotes #OK *
6. folding
7. taglist, ctags #OK *
8. sessions (save window, tabs, etc state) #OK *
9. projects (project plugin || nerdcomander)
10. fragments (example, try catch construction) #OK *
11. tab omnicomplete #OK *
12. class docs
13. android xml
    * easy work with xml tags #OK
    * android tags help
14. auto import
15. plugin manager #OK
